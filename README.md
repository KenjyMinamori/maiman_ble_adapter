## Firmware 
This firmware is for ESP32 family chips (esp-idf v3.2.2)
There is no additional files needed, only `main.h` and `main.c` so you can easily build it with
any esp-idf toolchain (driven make or cmake). 

This firmware is based on `bluetooth/ble_spp_server` example
The main differ is separated UART for communication. meanwhile UART0 is only
for debugging. 

### RU
Эта прошивка основана на примере из библиотеки `bluetooth/ble_spp_server`. 
Главное отличие от примера - для передачи данных по UART используется отдельный 
порт, тогда как UART0 используется только для вывода дебажной информации

Также характеристикам даны UUID, соответсвующие нордиковскому протоколу.

## HW

### Uart for main communication: 
* UART_TXD : GPIO_4
* UART_RXD : GPIO_5

### Uart for debug and flashing:
Standart UART0 


## Protocol

This device uses Nordic UART protocole that emulates UART via BLE GATT service. 

* Service UUID: `6E400001-B5A3-F393-E0A9-E50E24DCCA9E` 
* RX Characteristic: `6E400002-B5A3-F393-E0A9-E50E24DCCA9E`
* TX Characteristic: `6E400003-B5A3-F393-E0A9-E50E24DCCA9E`

This protocol is simple: send your data to RX Characteristic, and wait incoming data from TX.

Also, you should note that MTU size for this app is 512 byte (!) It works perfect on Android, please check it carefully on iOS.

Another note: this protocol is ready for MTU overflow. If incomming data to device's harware uart is too big ( > 512b) it will split it to packets


|  0  |  1  |   2      |     3      |  4 .. (mtu size) |
| --- | --- | -------- | ---------- | ---------------- |
| '#' | '#' | totalNum | currentNum |  packet          |

## BLE status 
GPIO 2 shows status: 
* HIGH : ble is connected
* LOW  : ble is disconnected

## How to flash it

First of all, download CI output. 

There are 3 important for us files:
* bootloader.bim - should be placed to `0x1000`
* partioion_table.bin - should be placed to `0x8000` 
* ble_spp_server_demo.bin - should be placed to  `0x10000`


You need `Flash Download Tool` from espressif (I added it dirrectly to the repo in case of troubles): 

* [Download page](https://www.espressif.com/en/support/download/other-tools)
* [Direct link to soft](https://www.espressif.com/sites/default/files/tools/flash_download_tools_v3.6.7_1.zip)

Don't forget to place flasher and binaries to the proper path (without cyrillic symbols) 

All other settings should be default. Additional pager


## How to build it
[Official guide](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html)
This project uses "new" environment with CMake which may be unstable for now. 
Maybe in future it won't work (it should), so there is a guide how to fix it.

Anyway it's not a big deal, this project uses only one main.c file and the header for it, so 
if you have problems with 4.0 or higher versions, just copy the source code to the proper 
example and it will work.